ssh_config
	cp ssh_config ~/.ssh/config

	%s/<IDSID>/Your_IDSID
	%s/<home dir path>/Your_home_dir_path
	%s/<email_id_wo_.intel.com>/

gitconfig
	cp gitconfig ~/.gitconfig

	And replace below strings:
		<IDSID>
		<intel mail id without @intel.com>
		<Name>
		<<Mail_ID>>

connect and socks-gw
	copy them into the /usr/bin/ and mark them as executable

muttrc
	cp muttrc_generic ~/.muttrc

	%s/<Real Name>/Your name
	%s/<mail_id>/Your mail id
	%s/<IDSID>/Your IDSID
	%s/<absolute path of .muttrc_sidebar>/Absolute path of .muttrc_sidebar

Irssi
	cp irssi_config ~/.irssi/config

	%s/<Real Name>/ Your_name
	%s/<passwd>/Your_passwd
	%s/<nickname>/Your_nickname

tsocks:
	cp tsocks.conf /etc/tsocks.conf

proxychain
	sudo cp proxychain* /etc/

tmux:
	cp .tmux.conf ~/.tmux.conf

muttrc_sidebar
	cp muttrc_sidebar_generic ~/.muttrc_sidebar

	Update the folder names as per your folders
